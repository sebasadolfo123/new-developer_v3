FROM python:3.11.4

ADD main.py .

CMD [ "python", "./main.py" ]