import unittest

def encontrar_par_suma_binaria(arr, suma):
    n = len(arr)
    
    for i in range(n - 1):
        complemento = suma - arr[i]
        inicio = i + 1
        fin = n - 1
        
        while inicio <= fin:
            medio = (inicio + fin) // 2
            
            if arr[medio] == complemento:
                return (arr[i], arr[medio])
            elif arr[medio] < complemento:
                inicio = medio + 1
            else:
                fin = medio - 1
    
    return "No se encontró ningún par que coincida con la suma dada"


class TestEncontrarParSumaBinaria(unittest.TestCase):
    def test_encontrar_par_suma_binaria(self):
        arr1 = [2, 3, 6, 7, 8]
        suma1 = 9
        self.assertEqual(encontrar_par_suma_binaria(arr1, suma1), (2, 7))

if __name__ == '__main__':
    unittest.main()